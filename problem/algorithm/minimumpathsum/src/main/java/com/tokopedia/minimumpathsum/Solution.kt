package com.tokopedia.minimumpathsum

object Solution {
    fun minimumPathSum(matrix: Array<IntArray>): Int {
        // TODO, find a path from top left to bottom right which minimizes the sum of all numbers along its path, and return the sum
        // below is stub
        for (i in 1..matrix.lastIndex) {
            matrix[i][0] = matrix[i-1][0] + matrix[i][0]
        }
        for (j in 1..matrix.last().lastIndex) {
            matrix[0][j] = matrix[0][j-1] + matrix[0][j]
        }

        for (i in 1..matrix.lastIndex) {
            for (j in 1..matrix.last().lastIndex) {
                matrix[i][j] = minOf(matrix[i-1][j], matrix[i][j-1]) + matrix[i][j]
            }
        }
        return matrix.last().last()
    }

}
