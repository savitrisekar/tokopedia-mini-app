package com.tokopedia.climbingstairs

object Solution {
    fun climbStairs(n: Int): Long {
        // TODO, return in how many distinct ways can you climb to the top. Each time you can either climb 1 or 2 steps.
        // 1 <= n < 90
        var dp = LongArray(n+1)
        dp[0] =1
        dp[1] = 1
        for(i in 2 until n+1) dp[i] = dp[i-1] + dp[i-2]
        return dp[n]
    }
}
