package com.tokopedia.oilreservoir

/**
 * Created by fwidjaja on 2019-09-24.
 */
object Solution {
    fun collectOil(height: IntArray): Int {
        // TODO, return the amount of oil blocks that could be collected
        // below is stub
        var left = 0
        var right = height.size - 1
        var sum = 0
        var leftMax = 0
        var rightMax = 0
        while (left < right) {
            if (height[left] < height[right]) {
                if (height[left] >= leftMax) {
                    leftMax = height[left]
                } else {
                    sum = sum + (leftMax - height[left])
                }
                left = left + 1
            } else {
                if (height[right] >= rightMax) {
                    rightMax = height[right]
                } else {
                    sum = sum + (rightMax - height[right])
                }
                right = right - 1
            }
        }
        return sum
    }
}
