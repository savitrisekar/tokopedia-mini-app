package com.tokopedia.filter.view.model

class DataShop(
        val id: Int,
        val name: String,
        val city: String
)