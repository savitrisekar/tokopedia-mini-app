package com.tokopedia.filter.view.adapter

import android.content.Context
import android.graphics.BitmapFactory
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.tokopedia.filter.R
import com.tokopedia.filter.view.model.DataProduct
import kotlinx.android.extensions.LayoutContainer
import kotlinx.android.synthetic.main.main_item.view.*
import java.io.IOException
import java.io.InputStream
import java.net.MalformedURLException
import java.net.URL
import java.text.DecimalFormat

class ProductAdapter(private val context: Context, private val productList: List<DataProduct>)
    : RecyclerView.Adapter<ProductAdapter.Holder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): Holder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.main_item, parent, false)
        return Holder(view)
    }

    override fun getItemCount(): Int {
        return productList.size
    }

    override fun onBindViewHolder(holder: Holder, position: Int) {
        holder.bindItem(productList[position])
    }

    class Holder(override val containerView: View) : RecyclerView.ViewHolder(containerView),
            LayoutContainer {

        private val df = DecimalFormat("###,###,###")

        fun bindItem(dataProduct: DataProduct) {
            containerView.tv_title.text = dataProduct.name
            containerView.tv_price.text = "Rp " + df.format(dataProduct.priceInt).replace(',', ',')
            containerView.tv_location.text = dataProduct.shop.city

            try {
                val bitmap = BitmapFactory.decodeStream(URL(dataProduct.imageUrl).content as InputStream)
                containerView.iv_image_product.setImageBitmap(bitmap)
            } catch (e: MalformedURLException) {
                e.printStackTrace()
            } catch (e: IOException) {
                e.printStackTrace()
            }
        }
    }
}
