package com.tokopedia.filter.view

import android.annotation.SuppressLint
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.os.StrictMode
import android.util.TypedValue
import android.view.LayoutInflater
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.GridLayoutManager
import com.google.android.material.chip.Chip
import com.google.android.material.chip.ChipGroup
import com.tokopedia.filter.R
import com.tokopedia.filter.view.adapter.ProductAdapter
import com.tokopedia.filter.view.model.DataProduct
import com.tokopedia.filter.view.model.DataShop
import kotlinx.android.synthetic.main.activity_product.*
import kotlinx.android.synthetic.main.product_item.view.*
import org.json.JSONObject
import java.io.ByteArrayOutputStream
import java.io.IOException
import java.io.InputStream
import java.lang.Exception
import java.text.DecimalFormat
import java.util.*
import kotlin.concurrent.schedule

class ProductActivity : AppCompatActivity() {

    private var dataProduct = mutableListOf<DataProduct>()
    private var city = mutableListOf<String>()
    private var prices = mutableListOf<Int>()
    private var filterPrice = mutableListOf<Float>()
    private var filterCity = mutableListOf<String>()

    private lateinit var productAdapter: ProductAdapter
    private lateinit var alert: AlertDialog.Builder
    private lateinit var loading: AlertDialog

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_product)

        val policy: StrictMode.ThreadPolicy = StrictMode.ThreadPolicy.Builder()
                .permitAll().build()
        StrictMode.setThreadPolicy(policy)

        setupAlert()
        dataProduct = initData()
        setupAdapter()
    }

    private fun setupAlert() {
        alert = AlertDialog.Builder(this)
        alert.setCancelable(false)
        alert.setView(R.layout.product_load_more)
        loading = alert.create()
        loading.window?.setBackgroundDrawable(ColorDrawable(android.graphics.Color.TRANSPARENT))

    }

    private fun setupAdapter() {
        productAdapter = ProductAdapter(this, dataProduct)
        product_list.layoutManager = GridLayoutManager(this, 2)
        product_list.adapter = productAdapter
    }

    @SuppressLint("SetTextI18n")
    override fun onResume() {
        super.onResume()

        fab_filter.setOnClickListener {
            val dialogView = LayoutInflater.from(this).inflate(R.layout.product_item, null)
            val builder = AlertDialog.Builder(this).setView(dialogView)
            val alertDialog = builder.setCancelable(false).show()

            val df = DecimalFormat("###,###,###")
            val max = Collections.max(prices)
            val min = Collections.min(prices)

            dialogView.price_max.text = "max\nRp. " + df.format(max).replace(',', '.')
            dialogView.price_min.text = "min\nRp. " + df.format(min).replace(',', '.')

            dialogView.price_slider.addOnChangeListener { slider, _, _ ->
                dialogView.price_max.text = "max\nRp. " + df.format(slider.values[1]).replace(',', '.')
                dialogView.price_min.text = "min\nRp. " + df.format(slider.values[0]).replace(',', '.')

            }

            dialogView.btn_cancel.setOnClickListener {
                alertDialog.dismiss()
            }

            dialogView.price_slider.valueTo = max.toFloat()
            dialogView.price_slider.valueFrom = min.toFloat()

            if (filterPrice.isEmpty())
                dialogView.price_slider.values = arrayListOf(min.toFloat(), max.toFloat())
            else
                dialogView.price_slider.values = arrayListOf(filterPrice[0], filterPrice[1])


            setCityChips(city, dialogView.chipsGroup)

            dialogView.btn_filter.setOnClickListener {
                filterPrice = dialogView.price_slider.values
                loading.show()

                val filter = filterData(initData(), filterCity, filterPrice)
                dataProduct.clear()
                dataProduct.addAll(filter)

                Timer("Waiting..", false).schedule(1000) {
                    product_list.post {
                        productAdapter.notifyDataSetChanged()
                    }
                    loading.dismiss()
                    alertDialog.dismiss()
                }
            }
        }
    }

    @SuppressLint("InflateParams")
    private fun setCityChips(listCity: MutableList<String>, chipsGroup: ChipGroup) {
        for (city in listCity) {

            val chip = this.layoutInflater.inflate(R.layout.chips_item, null, false) as Chip
            val padding = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 10f, resources.displayMetrics).toInt()

            chip.text = city
            chip.setPadding(padding, 0, padding, 0)

            if (filterCity.isNotEmpty())
                if (filterCity.contains(city))
                    chip.isChecked = true

            chip.setOnCheckedChangeListener { _, b ->
                if (b)
                    filterCity.add(chip.text.toString())
                else
                    filterCity.remove(chip.text.toString())

            }

            chipsGroup.addView(chip)
        }
    }

    private fun filterData(
            product: MutableList<DataProduct>,
            list_city: MutableList<String>,
            price: MutableList<Float>): Collection<DataProduct> {

        val filterProduct = mutableListOf<DataProduct>()

        for (data in product) {
            if (list_city.isNotEmpty())

                for (city in list_city) {
                    if (data.shop.city == city && data.priceInt >= price[0] && data.priceInt <= price[1]) {
                        filterProduct.add(data)
                    }
                }
            else {
                if (data.priceInt >= price[0] && data.priceInt <= price[1]) {
                    filterProduct.add(data)
                }
            }
        }
        return filterProduct
    }

    private fun initData(): MutableList<DataProduct> {
        val product = mutableListOf<DataProduct>()
        val inputStream: InputStream = resources.openRawResource(R.raw.products)
        val byteArrayOutputStream = ByteArrayOutputStream()

        var ctr: Int
        try {
            ctr = inputStream.read()
            while (ctr != -1) {
                byteArrayOutputStream.write(ctr)
                ctr = inputStream.read()
            }
            inputStream.close()
        } catch (e: IOException) {
            e.printStackTrace()
        }
        try {
            val jObject = JSONObject(byteArrayOutputStream.toString())
            val result = jObject.getJSONObject("data")
            val arrayProduct = result.getJSONArray("products")

            for (i in 0 until arrayProduct.length()) {
                val jShop = arrayProduct.getJSONObject(i).getJSONObject("shop")
                val countries = jShop.getString("city")
                val price = arrayProduct.getJSONObject(i).getInt("priceInt")

                if (!city.contains(countries))
                    city.add(countries)

                if (!prices.contains(price))
                    prices.add(price)

                product.add(
                        DataProduct(
                                arrayProduct.getJSONObject(i).getInt("id"),
                                arrayProduct.getJSONObject(i).getString("name"),
                                arrayProduct.getJSONObject(i).getString("imageUrl"),
                                arrayProduct.getJSONObject(i).getInt("priceInt"),
                                arrayProduct.getJSONObject(i).getInt("discountPercentage"),
                                arrayProduct.getJSONObject(i).getInt("slashedPriceInt"),
                                DataShop(
                                        jShop.getInt("id"),
                                        jShop.getString("name"),
                                        jShop.getString("city")
                                )
                        )
                )
            }

        } catch (e: Exception) {
            e.printStackTrace()
        }
        return product
    }
}