package com.tokopedia.maps.network

import com.tokopedia.maps.utils.Constans
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

class ApiClient {
    fun getInterceptor(): OkHttpClient.Builder {
        val clientBuilder = OkHttpClient.Builder()
        val httpLoggingInterceptor = HttpLoggingInterceptor()
        httpLoggingInterceptor.level = HttpLoggingInterceptor.Level.BODY

        clientBuilder.addInterceptor(httpLoggingInterceptor)
                .connectTimeout(30, TimeUnit.SECONDS)
                .writeTimeout(30, TimeUnit.SECONDS)
                .retryOnConnectionFailure(true)
                .build()

        return clientBuilder
    }

    fun apiClient(): Retrofit {
        val clientBuilder: OkHttpClient.Builder = getInterceptor()
        return Retrofit.Builder().baseUrl(Constans.BASE_URL)
                .client(clientBuilder
                        .addInterceptor {
                            chain ->
                            val newRequest = chain.request().newBuilder()
                                    .addHeader("x-rapidapi-key", Constans.rapidApiKey)
                                    .addHeader("x-rapidapi-host", Constans.rapidApiHost)
                                    .build()
                            chain.proceed(newRequest)
                        }
                        .retryOnConnectionFailure(false)
                        .build()
                )
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build()
    }

    fun api(): ApiService {
        return apiClient().create(ApiService::class.java)
    }
}