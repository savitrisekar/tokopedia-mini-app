package com.tokopedia.maps

import android.app.ProgressDialog
import android.content.Context
import android.content.DialogInterface
import android.os.Bundle
import android.util.Log
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.EditText
import android.widget.TextView
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.BitmapDescriptorFactory
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import com.google.gson.JsonParser
import com.tokopedia.maps.model.Country
import com.tokopedia.maps.network.ApiClient
import com.tokopedia.maps.network.ApiService
import io.reactivex.Scheduler
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import retrofit2.HttpException
import java.util.*

open class MapsActivity : AppCompatActivity() {

    private var mapFragment: SupportMapFragment? = null
    private var googleMap: GoogleMap? = null

    private lateinit var textCountryName: TextView
    private lateinit var textCountryCapital: TextView
    private lateinit var textCountryPopulation: TextView
    private lateinit var textCountryCallCode: TextView

    private var editText: EditText? = null
    private var buttonSubmit: View? = null
    private var progressDialog: ProgressDialog? = null

    private var compositeDisposable: CompositeDisposable? = null

    private val apiClient = ApiClient().api()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_maps)
        bindViews()
        initListeners()
        loadMap()
    }

    private fun bindViews() {
        mapFragment = supportFragmentManager.findFragmentById(R.id.map) as SupportMapFragment
        editText = findViewById(R.id.editText)
        buttonSubmit = findViewById(R.id.buttonSubmit)
        textCountryName = findViewById(R.id.txtCountryName)
        textCountryCapital = findViewById(R.id.txtCountryCapital)
        textCountryPopulation = findViewById(R.id.txtCountryPopulation)
        textCountryCallCode = findViewById(R.id.txtCountryCallCode)
    }

    private fun initListeners() {
        buttonSubmit!!.setOnClickListener {
            // TODO
            // search by the given country name, and
            // 1. pin point to the map
            // 2. set the country information to the textViews.

            val searchCountry = editText?.text.toString()
            loadLocation(searchCountry)
        }
    }

    private fun loadLocation(searchCountry: String) {

        progressDialog = ProgressDialog.show(this, null, "Searching data ...", true, false)

        compositeDisposable = CompositeDisposable()

        val apiService: ApiService = apiClient

        compositeDisposable?.add(apiService.getByCountryName(searchCountry)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(this::locationResponse, this::locationError)
        )
    }

    private fun locationResponse(countryList: List<Country>) {
        val countryName = editText?.text.toString()

        for (data in countryList) {
            if (countryName.toLowerCase(Locale.getDefault()) == data.name.toLowerCase(Locale.getDefault())) {
                progressDialog!!.dismiss()
                setInformation(data)
                pinPointToMap(data.name, data.latlng[0], data.latlng[1])
                break
            } else {
                for (altSpelling in data.altSpellings) {
                    if (countryName.toLowerCase(Locale.getDefault()) == altSpelling.toLowerCase(Locale.getDefault())) {
                        progressDialog!!.dismiss()
                        setInformation(data)
                        pinPointToMap(data.name, data.latlng[0], data.latlng[1])
                        break
                    }
                }
            }
        }

        progressDialog!!.dismiss()
    }


    fun loadMap() {
        mapFragment!!.getMapAsync { googleMap -> this@MapsActivity.googleMap = googleMap }
    }

    private fun setInformation(data: Country) {
        textCountryName.text = "Nama negara: " + data.name
        textCountryCapital.text = "Ibukota: " + data.capital
        textCountryPopulation.text = "Jumlah penduduk: " + data.population
        textCountryCallCode.text = "Kode telepon: " + data.callingCodes
    }

    private fun pinPointToMap(location: String, lat: Double, lng: Double) {
        //TODO("pin point to the map")
        val latLng = LatLng(lat, lng)
        googleMap!!.clear()
        googleMap!!.addMarker(MarkerOptions()
                .position(latLng).title(location)
                .icon(BitmapDescriptorFactory
                        .fromResource(R.drawable.ic_position)))
        googleMap!!
                .animateCamera(CameraUpdateFactory
                        .newLatLngZoom(latLng, 3.0F))

    }

    private fun showDialog(message: String) {
        val builder = AlertDialog.Builder(this)
        builder.setMessage(message)
                .setPositiveButton("OK", dialogClickListener)
                .setCancelable(false).show()
    }

    private val dialogClickListener = DialogInterface.OnClickListener { dialog, which ->
        when (which) {
            DialogInterface.BUTTON_POSITIVE -> {
                dialog.dismiss()
            }
        }
    }

    private fun locationError(error: Throwable) {
        //TODO("Handle the error response from the API")

        var message = "Error"
        progressDialog!!.dismiss()
        if (error is HttpException) {
            val data = error.response()?.errorBody()?.string()
            message = JsonParser().parse(data).asJsonObject["message"].asString

            if (message.toLowerCase().contains("not found")) {
                showDialog("Country not found, Please fill in completely")
            } else {
                showDialog("Error : " + message)
            }

        } else {
            message = error.localizedMessage
            showDialog(message)
        }
        Log.d("TAG", error.localizedMessage)
    }
}
