package com.tokopedia.maps.network

import com.tokopedia.maps.model.Country
import io.reactivex.Observable
import retrofit2.http.GET
import retrofit2.http.Path

interface ApiService {

    @GET("name/{country}")
    fun getByCountryName(@Path("country") country: String): Observable<List<Country>>
}